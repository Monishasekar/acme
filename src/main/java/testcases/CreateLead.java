package testcases;


import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;


import CreateLead.Annotations;

public class CreateLead extends Annotations {
@Test
    public void createLead(){
	
	    WebElement eleCrmsfa = locateElement("xpath","//div[@id = 'label']/a");
		click(eleCrmsfa);
    	WebElement eleCreateLead = locateElement("link", "Create Lead");
	    click(eleCreateLead);
		
		WebElement eleCmpnyName = locateElement("id","createLeadForm_companyName");
		clearAndType(eleCmpnyName, "Wipro");
		WebElement eleFirstName = locateElement("id","createLeadForm_firstName");
		clearAndType(eleFirstName, "Monisha");
		WebElement eleLastName = locateElement("id","createLeadForm_lastName");
		clearAndType(eleLastName, "Sekar");
	    WebElement eleDropdownSource = locateElement("id","createLeadForm_dataSourceId");
	    selectDropDownUsingIndex(eleDropdownSource,1);
		WebElement eleSubmitCreateLead = locateElement("xpath","//input[@name='submitButton']");
		click(eleSubmitCreateLead);
		WebElement eleVerify = locateElement("id","viewLead_companyName_sp");
		verifyPartialText(eleVerify, "Wipro");
		
}
}